//
//  DashboardVC.swift
//  MantraMelody
//
//  Created by Nick Joliya on 24/08/23.
//

import UIKit
class DashboardVC: UIViewController {

    @IBOutlet weak var cvMainList: UICollectionView!
    var data : [MenuManager.MenuItem] = []
    override func viewDidLoad() {
        super.viewDidLoad()

        data = MenuManager.shared.menuItems
        cvMainList.delegate = self
        cvMainList.dataSource = self
    }
}

extension DashboardVC : UICollectionViewDataSource ,UICollectionViewDelegate , UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CVC_Dashboard", for: indexPath) as! CVC_Dashboard
        cell.lblTitle.text = data[indexPath.row].title
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: cvMainList.frame.size.width /  2 , height: 150)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 1 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "TrendingBooksVC") as! TrendingBooksVC
            self.navigationController?.pushViewController(vc, animated: true)
        }else if indexPath.row == 0 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "LatestBooksVC") as! LatestBooksVC
            self.navigationController?.pushViewController(vc, animated: true)
        }else if indexPath.row == 3 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PageFlowChoiceVC") as! PageFlowChoiceVC
            self.navigationController?.pushViewController(vc, animated: true)
        }else if indexPath.row == 2 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ReadBookMainVC") as! ReadBookMainVC
            self.navigationController?.pushViewController(vc, animated: true)
        }else if indexPath.row == 4 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AboutusVC") as! AboutusVC
            self.navigationController?.pushViewController(vc, animated: true)
        }else if indexPath.row == 5 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ContactUsVC") as! ContactUsVC
            self.navigationController?.pushViewController(vc, animated: true)
        }else if indexPath.row == 6 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PrivacyVC") as! PrivacyVC
            self.navigationController?.pushViewController(vc, animated: true)
        }else if indexPath.row == 7 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "IntroVC") as! IntroVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
}
