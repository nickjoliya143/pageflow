//
//  ScoreVC.swift
//  PokerKing
//
//  Created by Apple on 11/08/23.
//

import UIKit

class ScoreVC: UIViewController {


    @IBOutlet weak var imgBg: UIImageView!
    
    @IBOutlet weak var lblPoints: UILabel!
    
    let def = UserDefaults.standard
    
    var myPoints = 0{
        didSet{
            lblPoints.text = """
YOUR POKER POINTS ARE.

\(myPoints)♣️
"""
            def.set(myPoints, forKey: "point")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let myPoint = def.value(forKey: "point"),  myPoint as! Int != 0{
            self.myPoints = myPoint as! Int
        }else{
            myPoints = 10000
        }
        
        //imgBg.image = UIImage.gifImageWithName("g4")
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    @IBAction func btnAddPoint(_ sender: Any) {
        if myPoints < 500{
            myPoints = 5000
        }
    }
    

}
