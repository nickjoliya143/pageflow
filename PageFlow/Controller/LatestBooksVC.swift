//
//  TattoIdea_VC.swift
//  MantraMelody
//
//  Created by Nick Joliya on 26/08/23.
//

import UIKit

class LatestBooksVC: UIViewController {
    
    
    @IBOutlet weak var cvTrandingList: UICollectionView!
    var tattoIdeaData : [MenuManager.MenuItem] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tattoIdeaData = MenuManager.shared.LatestBooks
        cvTrandingList.delegate = self
        cvTrandingList.dataSource = self
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension LatestBooksVC : UICollectionViewDataSource ,UICollectionViewDelegate , UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tattoIdeaData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CVCTrendingSlok", for: indexPath) as! CVCTrendingSlok
        cell.imgBg.image  = UIImage(named: tattoIdeaData[indexPath.row].title)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: cvTrandingList.frame.size.width / 2 , height: 250)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
    }
}

