//
//  LearnVC.swift
//  MantraMelody
//
//  Created by Nick Joliya on 26/08/23.
//


import UIKit
import PDFKit

class ReadBookVC: UIViewController, UIScrollViewDelegate {

    @IBOutlet weak var lblBookTitle: UILabel!
    @IBOutlet weak var pdfScrollView: UIScrollView!
    var pdfDocument: PDFDocument!
    var currentPageIndex = 0
    var bookname = ""
    var pdfName = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        
        lblBookTitle.text = bookname
        // Load your PDF document
        if let pdfURL = Bundle.main.url(forResource: pdfName, withExtension: "pdf") {
            pdfDocument = PDFDocument(url: pdfURL)
            configureScrollView()
            loadPage(at: currentPageIndex)
        }
    }

    func configureScrollView() {
        pdfScrollView.delegate = self
        pdfScrollView.isPagingEnabled = true
        pdfScrollView.showsHorizontalScrollIndicator = false
        pdfScrollView.showsVerticalScrollIndicator = false
    }

    func loadPage(at index: Int) {
        guard index >= 0 && index < pdfDocument.pageCount else {
            return
        }

        let page = pdfDocument.page(at: index)
        let pdfView = PDFView(frame: CGRect(x: CGFloat(index) * pdfScrollView.bounds.width, y: 0, width: pdfScrollView.bounds.width, height: pdfScrollView.bounds.height))
        pdfView.backgroundColor = .clear
        pdfView.document = pdfDocument
        pdfView.displayMode = .singlePage
        pdfView.displayDirection = .vertical
        pdfView.autoScales = true
        pdfView.go(to: page!)

        pdfScrollView.addSubview(pdfView)
        pdfScrollView.contentSize = CGSize(width: CGFloat(pdfDocument.pageCount) * pdfScrollView.bounds.width, height: pdfScrollView.bounds.height)
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageIndex = Int(scrollView.contentOffset.x / scrollView.bounds.width)
        
        if pageIndex != currentPageIndex {
            currentPageIndex = pageIndex
            
            UIView.transition(with: pdfScrollView, duration: 0.3, options: .transitionCurlUp, animations: {
                self.pdfScrollView.subviews.forEach { $0.removeFromSuperview() }
                self.loadPage(at: self.currentPageIndex)
            })
        }
    }
    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}


