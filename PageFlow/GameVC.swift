//
//  GameVC.swift
//  PokerKing
//
//  Created by Apple on 11/08/23.
//

import UIKit

class GameVC: UIViewController {

    @IBOutlet weak var imgBg: UIImageView!
    
    @IBOutlet weak var lblTotalBid: UILabel!
    
    @IBOutlet weak var btnMyPoints: UIButton!
    
    @IBOutlet weak var lblBotCard: UILabel!
    
    @IBOutlet weak var lblMyCard: UILabel!
    
    let def = UserDefaults.standard
    
    
    let AllC = [
        ["🂢", "🂣", "🂤", "🂥", "🂦",  "🂧", "🂨", "🂩", "🂪", "🂫", "🂭", "🂮", "🂡"], // Spades
        ["🂲", "🂳", "🂴", "🂵", "🂶", "🂷", "🂸", "🂹", "🂺", "🂻", "🂽", "🂾", "🂱"], // Hearts
        ["🃂", "🃃", "🃄", "🃅", "🃆", "🃇", "🃈", "🃉", "🃊", "🃋", "🃍", "🃎", "🃁"], // Diamonds
        ["🃒", "🃓", "🃔", "🃕", "🃖", "🃗", "🃘", "🃙", "🃚", "🃛", "🃝", "🃞", "🃑"]  // Clubs
    ]
    
    var arrCards: [String] = []
    
    var totalBid = 0 {
        didSet{
            
            if totalBid == 0{
                lblTotalBid.text = "000"
                return
            }
            
            lblTotalBid.text = "\(totalBid)"
            
        }
    }
    
    var botCard  = "🃟🃟🃟"
    
    var isShow: Bool = false{
        didSet{
            
//            if let myPoint = def.value(forKey: "point"){
//                self.myPoints = myPoint as! Int
//            }else{
//                myPoints = 10000
//            }
            
            if isShow{
                lblBotCard.text = botCard
            }else{
                lblBotCard.text = "🃟🃟🃟"
            }
            
        }
    }
    
    var myPoints: Int = 300{
        didSet{
            
            if myPoints < 150{
                let vc = storyboard?.instantiateViewController(withIdentifier: "ScoreVC")as! ScoreVC
                navigationController?.pushViewController(vc, animated: true)
                return
            }
            
            btnMyPoints.setTitle("\(myPoints)", for: .normal)
            def.setValue(myPoints, forKey: "point")
        }
    }
    
    var myId = 0{
        didSet{
            print("MyId:",myId)
            lblMyCard.text = arrCards[myId]
        }
    }
    
    var botId = 0 {
        didSet{
            print("BotId:",botId)
            botCard = arrCards[botId]
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        imgBg.image = UIImage.gifImageWithName("g4")
        
        if let myPoint = def.value(forKey: "point"){
            self.myPoints = myPoint as! Int
        }else{
            myPoints = 10000
        }
        
        arrCards.removeAll()
        
        for i in 0...12{
            arrCards.append("\(AllC[0][12-i])\(AllC[1][12-i])\(AllC[2][12-i])")
        }
        
        for i in 0...10{
            arrCards.append("\(AllC[0][12-i])\(AllC[1][12-i-1])\(AllC[2][12-i-2])")
        }
        
        for i in 0...5{
            arrCards.append("\(AllC[0][12-i])\(AllC[0][12-i-1])\(AllC[0][12-i-2])")
        }
        
        setNewTable()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        if let myPoint = def.value(forKey: "point"){
            self.myPoints = myPoint as! Int
        }else{
            myPoints = 10000
        }
        
    }
    
    @IBAction func btnAdd(_ sender: UIButton) {
//        print(arrCards)
        myPoints -= 100
        totalBid += 200
    }
    
    @IBAction func btnShow(_ sender: UIButton) {
        
        isShow = true
        
        if myId <= botId{
            print("winner")
            myPoints += totalBid
            titleAlert(str: "You Win.")
        }else{
            print("loser")
            titleAlert(str: "Bot Win.")
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1){
            
            self.askForNextTable()
            
        }
    }
    
    func titleAlert(str: String){
//        let oldTitle = self.title
        
        self.title = str
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1){
            self.title = "GAME"
        }
    }
    
    func setNewTable(){
        
        isShow = false
        
        myId = Array(0...arrCards.count-1).randomElement()!
        botId = Array(0...arrCards.count-1).randomElement()!
        
        if myId == botId{
            botId = Array(0...arrCards.count-1).randomElement()!
        }
        
        totalBid = 200
        
        myPoints -= 100
        
    }
    
    func askForNextTable(){
        
        let alertController = UIAlertController(title: "Start New Table.", message: "Starting Table Take 100 Points.", preferredStyle: .alert)
        let ActStart = UIAlertAction(title: "Start", style: .default, handler: { alert -> Void in
            self.setNewTable()
        })
        
        let ActHome = UIAlertAction(title: "Go Home", style: .destructive, handler: { alert -> Void in
            self.navigationController?.popToRootViewController(animated: true)
        })
        
        alertController.addAction(ActStart)
        alertController.addAction(ActHome)
        
        present(alertController, animated: true, completion: nil)
    }
    
}
