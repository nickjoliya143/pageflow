//
//  TextToVoiceVC.swift
//  MantraMelody
//
//  Created by Nick Joliya on 26/08/23.
//
import UIKit
import AVFoundation

class TextToVoiceVC: UIViewController {
    
    @IBOutlet weak var textView: UITextField!
    @IBOutlet weak var speakButton: UIButton!
    
    let synthesizer = AVSpeechSynthesizer()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        textView.text = "असतो मा सद्गमय"
    }
    
    @IBAction func speakAction(_ sender: UIButton) {
        guard let text = textView.text else { return }
        
        let speechUtterance = AVSpeechUtterance(string: text)
        speechUtterance.voice = AVSpeechSynthesisVoice(language: "hi-IN") // Hindi - India
        
        synthesizer.speak(speechUtterance)
    }
    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
}
