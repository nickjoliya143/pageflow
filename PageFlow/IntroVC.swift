//
//  IntroVC.swift
//  PokerKing
//
//  Created by Apple on 11/08/23.
//

import UIKit

class IntroVC: UIViewController {

    @IBOutlet weak var imgBg: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        imgBg.image = UIImage.gifImageWithName("g4")
        
        // Do any additional setup after loading the view.
    }
    
    

}
