//
//  TrendingSlokVC.swift
//  MantraMelody
//
//  Created by Nick Joliya on 24/08/23.
//

import UIKit

class TrendingSlokVC: UIViewController {
    
    
    @IBOutlet weak var cvTrandingList: UICollectionView!
    var trandingData : [MenuManager.MenuItem] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        trandingData = MenuManager.shared.trandingItems
        cvTrandingList.delegate = self
        cvTrandingList.dataSource = self
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension TrendingSlokVC : UICollectionViewDataSource ,UICollectionViewDelegate , UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return trandingData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CVCTrendingSlok", for: indexPath) as! CVCTrendingSlok
        cell.imgBg.image  = UIImage(named: trandingData[indexPath.row].title)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: cvTrandingList.frame.size.width , height: 200)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
        
    }
}
